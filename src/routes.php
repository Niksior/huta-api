<?php

use Illuminate\Database\Query\Builder;
use Ironworks\Controllers\MainController;
use Ironworks\Models\Product;
use Slim\Http\Request;
use Slim\Http\Response;

$container = $app->getContainer();
$controller = new MainController();
// Routes

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


///użytkownicy
$app->post('/api/v1/user', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->register($req, $res, $args, $container);
});
$app->post('/api/v1/login', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->login($req, $res, $args, $container);
});
$app->get('/api/v1/users', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->users($req, $res, $args, $container);
});
////--
///
///zamówienia
$app->get('/api/v1/orders', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getOrders($req, $res, $args, $container);
});
$app->post('/api/v1/order', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->addOrder($req, $res, $args, $container);
});
$app->put('/api/v1/order', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->updateOrder($req, $res, $args, $container);
});
////---
///
/// produkty
$app->get('/api/v1/products', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getProducts($req, $res, $args, $container);
});
////---
///
/// zasoby
$app->get('/api/v1/resources', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getResources($req, $res, $args, $container);
});
$app->put('/api/v1/resources', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->updateResources($req, $res, $args, $container);
});
////---
///
/// klienci
$app->get('/api/v1/clients', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getClients($req, $res, $args, $container);
});
$app->get('/api/v1/client/{id}', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getClient($req, $res, $args, $container);
});
$app->post('/api/v1/client', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->addClient($req, $res, $args, $container);
});
////---
///
/// adresy
$app->get('/api/v1/addresses', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getAddresses($req, $res, $args, $container);
});
$app->get('/api/v1/address/{id}', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getAddress($req, $res, $args, $container);
});
$app->post('/api/v1/address', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->addAddress($req, $res, $args, $container);
});
////---
///
/// skład
$app->get('/api/v1/compositions', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getCompositions($req, $res, $args, $container);
});
////---
///
/// szczegóły
$app->get('/api/v1/details/{id}', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->getDetails($req, $res, $args, $container);
});
$app->post('/api/v1/details', function (Request $req, Response $res, array $args) {
    global $controller, $container;
    return $controller->addDetails($req, $res, $args, $container);
});
///---------------
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
////cors
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});
