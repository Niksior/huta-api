<?php

namespace Ironworks\Models;

use DateTime;

class Order
{
    /** @var int $id_zamownienia */
    private $id_zamowienia;
    /** @var int $id_klienta */
    private $id_klienta;
    /** @var double $cena */
    private $cena;
    /** @var DateTime $data_dostawy*/
    private $data_dostawy;
    /** @var DateTime $data_platnosci */
    private $data_platnosci;
    /** @var string $adres_dostawy */
    private $adres_dostawy;
    /** @var string $adres_platnosci */
    private $adres_platnosci;
    /** @var bool $czy_zaplacone */
    private $czy_zaplacone;
    /** @var string $uwagi */
    private $uwagi;
    /** @var DateTime $data_zamowienia */
    private $data_zamowienia;

    /**
     * @return DateTime
     */
    public function getDataZamowienia(): DateTime
    {
        return $this->data_zamowienia;
    }

    /**
     * @param DateTime $data_zamowienia
     */
    public function setDataZamowienia(DateTime $data_zamowienia): void
    {
        $this->data_zamowienia = $data_zamowienia;
    }

    /**
     * @return int
     */
    public function getIdZamowienia(): int
    {
        return $this->id_zamowienia;
    }

    /**
     * @param int $id_zamowienia
     */
    public function setIdZamowienia(int $id_zamowienia): void
    {
        $this->id_zamowienia = $id_zamowienia;
    }

    /**
     * @return int
     */
    public function getIdKlienta(): int
    {
        return $this->id_klienta;
    }

    /**
     * @param int $id_klienta
     */
    public function setIdKlienta(int $id_klienta): void
    {
        $this->id_klienta = $id_klienta;
    }

    /**
     * @return float
     */
    public function getCena(): float
    {
        return $this->cena;
    }

    /**
     * @param float $cena
     */
    public function setCena(float $cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return DateTime
     */
    public function getDataDostawy(): DateTime
    {
        return $this->data_dostawy;
    }

    /**
     * @param DateTime $data_dostawy
     */
    public function setDataDostawy(DateTime $data_dostawy): void
    {
        $this->data_dostawy = $data_dostawy;
    }

    /**
     * @return DateTime
     */
    public function getDataPlatnosci(): DateTime
    {
        return $this->data_platnosci;
    }

    /**
     * @param DateTime $data_platnosci
     */
    public function setDataPlatnosci(DateTime $data_platnosci): void
    {
        $this->data_platnosci = $data_platnosci;
    }

    /**
     * @return string
     */
    public function getAdresDostawy(): string
    {
        return $this->adres_dostawy;
    }

    /**
     * @param string $adres_dostawy
     */
    public function setAdresDostawy(string $adres_dostawy): void
    {
        $this->adres_dostawy = $adres_dostawy;
    }

    /**
     * @return string
     */
    public function getAdresPlatnosci(): string
    {
        return $this->adres_platnosci;
    }

    /**
     * @param string $adres_platnosci
     */
    public function setAdresPlatnosci(string $adres_platnosci): void
    {
        $this->adres_platnosci = $adres_platnosci;
    }

    /**
     * @return bool
     */
    public function isCzyZaplacone(): bool
    {
        return $this->czy_zaplacone;
    }

    /**
     * @param bool $czy_zaplacone
     */
    public function setCzyZaplacone(bool $czy_zaplacone): void
    {
        $this->czy_zaplacone = $czy_zaplacone;
    }

    /**
     * @return string
     */
    public function getUwagi(): string
    {
        return $this->uwagi;
    }

    /**
     * @param string $uwagi
     */
    public function setUwagi(string $uwagi): void
    {
        $this->uwagi = $uwagi;
    }

    public function __invoke(): array
    {
        if ($this->id_zamowienia)
            return [
                'id_zamowienia' => $this->id_zamowienia,
                'id_klienta' => $this->id_klienta,
                'cena' => $this->cena,
                'data_dostawy' => $this->data_dostawy->format('Y-m-d H:i:s'),
                'data_platnosci' => $this->data_platnosci->format('Y-m-d H:i:s'),
                'data_zamowienia' => $this->data_zamowienia->format('Y-m-d H:i:s'),
                'adres_dostawy' => $this->adres_dostawy,
                'adres_platnosci' => $this->adres_platnosci,
                'czy_zaplacone' => $this->czy_zaplacone,
                'uwagi' => $this->uwagi
            ];
        return [
            'id_klienta' => $this->id_klienta,
            'cena' => $this->cena,
            'data_dostawy' => $this->data_dostawy->format('Y-m-d H:i:s'),
            'data_platnosci' => $this->data_platnosci->format('Y-m-d H:i:s'),
            'adres_dostawy' => $this->adres_dostawy,
            'adres_platnosci' => $this->adres_platnosci,
            'czy_zaplacone' => $this->czy_zaplacone,
            'uwagi' => $this->uwagi
        ];
    }

    static function createFromResponse(object $item): Order {
        $order = new Order();
        if ($item->data_platnosci)
            $order->setDataPlatnosci(new DateTime($item->data_platnosci));
        if ($item->data_zamowienia)
            $order->setDataZamowienia(new DateTime($item->data_zamowienia));
        $order->setIdZamowienia(intval($item->id_zamowienia));
        $order->setIdKlienta(intval($item->id_klienta));
        $order->setCena(floatval($item->cena));
        $order->setDataDostawy(new DateTime($item->data_dostawy));
        $order->setAdresDostawy($item->adres_dostawy);
        $order->setAdresPlatnosci($item->adres_platnosci);
        $order->setCzyZaplacone($item->czy_zaplacone);
        $order->setUwagi($item->uwagi);
        return $order;
    }
}