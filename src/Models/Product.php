<?php

namespace Ironworks\Models;

class Product
{

    /** @var string */
    private $nazwa_produktu;
    /** @var float */
    private $cena;
    /** @var float */
    private $czas_produkcji;

    /**
     * @return string
     */
    public function getNazwaStopu(): string
    {
        return $this->nazwa_produktu;
    }

    /**
     * @param string $nazwa_produktu
     */
    public function setNazwaStopu(string $nazwa_produktu): void
    {
        $this->nazwa_produktu = $nazwa_produktu;
    }

    /**
     * @return float
     */
    public function getCena(): float
    {
        return $this->cena;
    }

    /**
     * @param float $cena
     */
    public function setCena(float $cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return float
     */
    public function getCzasProdukcji(): float
    {
        return $this->czas_produkcji;
    }

    /**
     * @param float $czas_produkcji
     */
    public function setCzasProdukcji(float $czas_produkcji): void
    {
        $this->czas_produkcji = $czas_produkcji;
    }

    public function __construct($array)
    {
        $this->cena = floatval($array->cena);
        $this->czas_produkcji = floatval($array->czas_produkcji);
        $this->nazwa_produktu = $array->nazwa_produktu;
    }

    public function __invoke(): array
    {
        return [
            'cena' => $this->cena,
            'czas_produkcji' => $this->czas_produkcji,
            'nazwa_produktu' => $this->nazwa_produktu
        ];
    }

}