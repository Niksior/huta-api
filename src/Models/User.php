<?php

namespace Ironworks\Models;

class User
{
    /**
     * @var string $login
     */
    private $login;
    /**
     * @var string $password
     */
    private $password;
    /**
     * @var string $bearer
     */
    private $bearer;
    /**
     * @var bool $administrator
     */
    private $administrator;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getBearer(): string
    {
        return $this->bearer;
    }

    /**
     * @param string $bearer
     */
    public function setBearer(string $bearer): void
    {
        $this->bearer = $bearer;
    }

    /**
     * @return bool
     */
    public function isAdministrator(): bool
    {
        return $this->administrator;
    }

    /**
     * @param bool $administrator
     */
    public function setAdministrator(bool $administrator): void
    {
        $this->administrator = $administrator;
    }

    public function __invoke(): array
    {
        return [
            'login' => $this->login,
            'haslo' => $this->password,
            'administrator' => $this->administrator,
            'bearer' => $this->bearer
        ];
    }

    public function createFromResponse(object $item): void {
        $this->login = $item->login;
        $this->password = $item->haslo;
        $this->administrator = $item->administrator;
        $this->bearer = $item->bearer;
    }

}