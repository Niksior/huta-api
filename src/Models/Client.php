<?php

namespace Ironworks\Models;

use DateTime;

class Client
{
    /** @var int $id_klienta */
    private $id_klienta;
    /** @var string $imie */
    private $imie;
    /** @var string $nazwisko */
    private $nazwisko;
    /** @var string $nazwa_firmy */
    private $nazwa_firmy;
    /** @var string $numer_telefonu */
    private $numer_telefonu;
    /** @var string $email */
    private $email;
    /** @var DateTime */
    private $data_rejestracji;

    /**
     * @return int
     */
    public function getIdKlienta(): int
    {
        return $this->id_klienta;
    }

    /**
     * @param int $id_klienta
     */
    public function setIdKlienta(int $id_klienta): void
    {
        $this->id_klienta = $id_klienta;
    }

    /**
     * @return string
     */
    public function getImie(): string
    {
        return $this->imie;
    }

    /**
     * @param string $imie
     */
    public function setImie(string $imie): void
    {
        $this->imie = $imie;
    }

    /**
     * @return string
     */
    public function getNazwisko(): string
    {
        return $this->nazwisko;
    }

    /**
     * @param string $nazwisko
     */
    public function setNazwisko(string $nazwisko): void
    {
        $this->nazwisko = $nazwisko;
    }

    /**
     * @return string
     */
    public function getNazwaFirmy(): string
    {
        return $this->nazwa_firmy;
    }

    /**
     * @param string $nazwa_firmy
     */
    public function setNazwaFirmy(string $nazwa_firmy): void
    {
        $this->nazwa_firmy = $nazwa_firmy;
    }

    /**
     * @return string
     */
    public function getNumerTelefonu(): string
    {
        return $this->numer_telefonu;
    }

    /**
     * @param string $numer_telefonu
     */
    public function setNumerTelefonu(string $numer_telefonu): void
    {
        $this->numer_telefonu = $numer_telefonu;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getDataRejestracji(): DateTime
    {
        return $this->data_rejestracji;
    }

    /**
     * @param DateTime $data_rejestracji
     */
    public function setDataRejestracji(DateTime $data_rejestracji): void
    {
        $this->data_rejestracji = $data_rejestracji;
    }

    public function __invoke(): array
    {
        return [
            'imie' => $this->imie,
            'nazwisko' => $this->nazwisko,
            'nazwa_firmy' => $this->nazwa_firmy,
            'numer_telefonu' => $this->numer_telefonu,
            'email' => $this->email
        ];
    }

    public function createFromResponse(object $item): void {
        $this->id_klienta = intval($item->id_klienta);
        $this->imie = $item->imie;
        $this->nazwisko = $item->nazwisko;
        $this->nazwa_firmy =$item->nazwa_firmy;
        $this->numer_telefonu = $item->numer_telefonu;
        $this->email = $item->email;
        $this->data_rejestracji = $item->data_rejestracji;
    }

}