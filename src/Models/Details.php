<?php

namespace Ironworks\Models;


class Details
{
    /** @var int $id_zamowienia */
    private $id_zamowienia;
    /** @var string $nazwa_produktu */
    private $nazwa_produktu;
    /** @var double $cena */
    private $cena;
    /** @var double $ilosc */
    private $ilosc;

    /**
     * @return int
     */
    public function getIdZamowienia(): int
    {
        return $this->id_zamowienia;
    }

    /**
     * @param int $id_zamowienia
     */
    public function setIdZamowienia(int $id_zamowienia): void
    {
        $this->id_zamowienia = $id_zamowienia;
    }

    /**
     * @return string
     */
    public function getNazwaProduktu(): string
    {
        return $this->nazwa_produktu;
    }

    /**
     * @param string $nazwa_produktu
     */
    public function setNazwaProduktu(string $nazwa_produktu): void
    {
        $this->nazwa_produktu = $nazwa_produktu;
    }

    /**
     * @return float
     */
    public function getCena(): float
    {
        return $this->cena;
    }

    /**
     * @param float $cena
     */
    public function setCena(float $cena): void
    {
        $this->cena = $cena;
    }

    /**
     * @return float
     */
    public function getIlosc(): float
    {
        return $this->ilosc;
    }

    /**
     * @param float $ilosc
     */
    public function setIlosc(float $ilosc): void
    {
        $this->ilosc = $ilosc;
    }

    public function __invoke(): array
    {
        return [
            'id_zamowienia' => $this->id_zamowienia,
            'nazwa_produktu' => $this->nazwa_produktu,
            'cena' => $this->cena,
            'ilosc' => $this->ilosc
        ];
    }

    static function createFromResponse(object $item): Details {
        $details = new Details();
        $details->setCena(floatval($item->cena));
        $details->setIlosc(floatval($item->ilosc));
        $details->setNazwaProduktu($item->nazwa_produktu);
        $details->setIdZamowienia(intval($item->id_zamowienia));
        return $details;
    }

    static function createFromBody(array $item): Details {
        $details = new Details();
        $details->setCena(floatval($item['_cena']));
        $details->setIlosc(floatval($item['_ilosc']));
        $details->setNazwaProduktu($item['_nazwa_produktu']);
        $details->setIdZamowienia(intval($item['_id_zamowienia']));
        return $details;
    }

}