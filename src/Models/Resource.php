<?php

namespace Ironworks\Models;

class Resource
{
    /** @var string $nazwa_zasobu */
    private $nazwa_zasobu;
    /** @var double $ilosc */
    private $ilosc;

    /**
     * @return string
     */
    public function getNazwaZasobu(): string
    {
        return $this->nazwa_zasobu;
    }

    /**
     * @param string $nazwa_zasobu
     */
    public function setNazwaZasobu(string $nazwa_zasobu): void
    {
        $this->nazwa_zasobu = $nazwa_zasobu;
    }

    /**
     * @return float
     */
    public function getIlosc(): float
    {
        return $this->ilosc;
    }

    /**
     * @param float $ilosc
     */
    public function setIlosc(float $ilosc): void
    {
        $this->ilosc = $ilosc;
    }

    public function __invoke(): array
    {
        return [
            'nazwa_zasobu' => $this->nazwa_zasobu,
            'ilosc' => $this->ilosc
        ];
    }

    public static function createFromResponse(object $item): Resource {
        $resource = new Resource();
        $resource->setIlosc(floatval($item->ilosc));
        $resource->setNazwaZasobu($item->nazwa_zasobu);
        return $resource;
    }

    public static function createFromBody(array $item): Resource {
        $resource = new Resource();
        $resource->setIlosc(floatval($item['ilosc']));
        $resource->setNazwaZasobu($item['nazwa_zasobu']);
        return $resource;
    }

}