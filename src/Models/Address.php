<?php

namespace Ironworks\Models;

class Address
{
    /** @var int $id_klienta */
    private $id_klienta;
    /** @var string $adres_dostawy */
    private $adres_dostawy;
    /** @var string $adres_platnosci */
    private $adres_platnosci;

    /**
     * @return int
     */
    public function getIdKlienta(): int
    {
        return $this->id_klienta;
    }

    /**
     * @param int $id_klienta
     */
    public function setIdKlienta(int $id_klienta): void
    {
        $this->id_klienta = $id_klienta;
    }

    /**
     * @return string
     */
    public function getAdresDostawy(): string
    {
        return $this->adres_dostawy;
    }

    /**
     * @param string $adres_dostawy
     */
    public function setAdresDostawy(string $adres_dostawy): void
    {
        $this->adres_dostawy = $adres_dostawy;
    }

    /**
     * @return string
     */
    public function getAdresPlatnosci(): string
    {
        return $this->adres_platnosci;
    }

    /**
     * @param string $adres_platnosci
     */
    public function setAdresPlatnosci(string $adres_platnosci): void
    {
        $this->adres_platnosci = $adres_platnosci;
    }

    public function __invoke(): array
    {
        return [
            'id_klienta' => $this->id_klienta,
            'adres_dostawy' => $this->adres_dostawy,
            'adres_platnosci' => $this->adres_platnosci
        ];
    }

    public function createFromResponse(object $item): void {
        $this->id_klienta= $item->id_klienta;
        $this->adres_dostawy = $item->adres_dostawy;
        $this->adres_platnosci = $item->adres_platnosci;
    }


}