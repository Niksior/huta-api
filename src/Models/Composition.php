<?php

namespace Ironworks\Models;

class Composition
{
    /** @var string $nazwa_produktu */
    private $nazwa_produktu;
    /** @var double $zloto */
    private $zloto;
    /** @var double $srebro */
    private $srebro;
    /** @var double $pallad */
    private $pallad;
    /** @var double $miedz */
    private $miedz;
    /** @var double $nikiel */
    private $nikiel;
    /** @var double $platyna */
    private $platyna;

    /**
     * @return float
     */
    public function getPlatyna(): float
    {
        return $this->platyna;
    }

    /**
     * @param float $platyna
     */
    public function setPlatyna(float $platyna): void
    {
        $this->platyna = $platyna;
    }


    /**
     * @return string
     */
    public function getNazwaProduktu(): string
    {
        return $this->nazwa_produktu;
    }

    /**
     * @param string $nazwa_produktu
     */
    public function setNazwaProduktu(string $nazwa_produktu): void
    {
        $this->nazwa_produktu = $nazwa_produktu;
    }

    /**
     * @return float
     */
    public function getZloto(): float
    {
        return $this->zloto;
    }

    /**
     * @param float $zloto
     */
    public function setZloto(float $zloto): void
    {
        $this->zloto = $zloto;
    }

    /**
     * @return float
     */
    public function getSrebro(): float
    {
        return $this->srebro;
    }

    /**
     * @param float $srebro
     */
    public function setSrebro(float $srebro): void
    {
        $this->srebro = $srebro;
    }

    /**
     * @return float
     */
    public function getPallad(): float
    {
        return $this->pallad;
    }

    /**
     * @param float $pallad
     */
    public function setPallad(float $pallad): void
    {
        $this->pallad = $pallad;
    }

    /**
     * @return float
     */
    public function getMiedz(): float
    {
        return $this->miedz;
    }

    /**
     * @param float $miedz
     */
    public function setMiedz(float $miedz): void
    {
        $this->miedz = $miedz;
    }

    /**
     * @return float
     */
    public function getNikiel(): float
    {
        return $this->nikiel;
    }

    /**
     * @param float $nikiel
     */
    public function setNikiel(float $nikiel): void
    {
        $this->nikiel = $nikiel;
    }

    public function __invoke(): array
    {
        return [
            'nazwa_produktu' => $this->nazwa_produktu,
            'zloto' => $this->zloto,
            'srebro' => $this->srebro,
            'pallad' => $this->pallad,
            'miedz' => $this->miedz,
            'nikiel' => $this->nikiel,
            'platyna' => $this->platyna
        ];
    }

    public function __construct(object $item) {
        $this->nazwa_produktu = $item->nazwa_produktu;
        $this->zloto = floatval($item->zloto);
        $this->srebro = floatval($item->srebro);
        $this->pallad = floatval($item->pallad);
        $this->miedz = floatval($item->miedz);
        $this->nikiel = floatval($item->nikiel);
        $this->platyna = floatval($item->platyna);
    }

}