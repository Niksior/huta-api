<?php

namespace Ironworks\Controllers;

use DateTime;
use Illuminate\Database\Query\Builder;
use Ironworks\Models\Address;
use Ironworks\Models\Client;
use Ironworks\Models\Composition;
use Ironworks\Models\Details;
use Ironworks\Models\Order;
use Ironworks\Models\Product;
use Ironworks\Models\Resource;
use Ironworks\Models\User;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class MainController
{
    function checkAuthorization(Request $req, ContainerInterface $container, bool $admin): bool
    {
        $token = $req->getHeaderLine('Authorization');
        if (!$token) {
            return false;
        }
        $token = ltrim($token, ' ');
        $token = explode(" ", $token)[1];
        /** @var Builder $table */
        $table = $container->get('db')->table('uzytkownicy');
        $response = $table->where('bearer', $token)->first();
        $user = new User();
        $user->createFromResponse($response);
        if (!$user) {
            return false;
        }
        if ($admin && !$user->isAdministrator()) {
            return false;
        }
        return true;
    }
    ///user
    function register(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, true)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        $login = $body['_login'];
        $password = $body['_haslo'];
        $isAdmin = $body['_administrator'];
        if (!$login || !$password) {
            return $res->withStatus(400)->withJson($body);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('uzytkownicy');
        $users = $table->where('login', $login)->first();
        if ($users) {
            return $res->withStatus(409);
        }
        $isAdmin = filter_var($isAdmin, FILTER_VALIDATE_BOOLEAN);
        $user = new User();
        $user->setLogin($login);
        $user->setPassword(password_hash($password, PASSWORD_BCRYPT));
        $user->setAdministrator($isAdmin);
        $user->setBearer(bin2hex(random_bytes(16)));
        $table->insert($user());
        return $res->withStatus(200);
    }
    function login(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        $body = $req->getParsedBody();
        $login = $body['login'];
        $password = $body['password'];
        if (!$login || !$password || sizeof($body) != 2) {
            return $res->withStatus(400);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('uzytkownicy');
        $retrievedUser = $table->where('login', $login)->first();
        if (!$retrievedUser) {
            return $res->withStatus(403);
        }
        if (!password_verify($password, $retrievedUser->haslo)) {
            print $retrievedUser->haslo;
            return $res->withStatus(401);
        }
        return $res->withStatus(200)->withJson($retrievedUser);
    }
    function users(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, true)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('uzytkownicy');
        return $res->withStatus(200)->withJson($table->orderBy('login')->get());
    }
    ///product
    function getProducts(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('produkty');
        $responseTable = $table->orderBy('nazwa_produktu')->get();
        $products = [];
        foreach ($responseTable as $item) {
            $product = new Product($item);
            array_push($products, $product());
        }
        return $res->withStatus(200)->withJson($products);
    }
    ///resource
    function getResources(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('zasoby');
        $responseTable = $table->orderBy('nazwa_zasobu')->get();
        $resources = [];
        foreach ($responseTable as $item) {
            $resource = Resource::createFromResponse($item);
            array_push($resources, $resource());
        }
        return $res->withStatus(200)->withJson($resources);
    }
    function updateResources(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('zasoby');
        $body = $req->getParsedBody();
        foreach ($body as $item) {
            $resource = Resource::createFromBody($item);
            $container->get('db')->table('zasoby')
                ->where('nazwa_zasobu', $resource->getNazwaZasobu())
                ->update(['ilosc' => $resource->getIlosc()]);
        }
        return $res->withStatus(200)->withJson($table->get());
    }
    ///client
    function getClients(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('klienci');
        return $res->withStatus(200)->withJson($table->orderBy('nazwisko')->get());
    }
    function addClient(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        $imie = $body['_imie'];
        $nazwisko = $body['_nazwisko'];
        $nazwa_firmy = $body['_nazwa_firmy'];
        $numer_telefonu = $body['_numer_telefonu'];
        $email = $body['_email'];

        if (!$imie || !$nazwisko || !$nazwa_firmy || !$numer_telefonu || !$email) {
            return $res->withStatus(400)->withJson($body);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('klienci');
        $clients = $table->where('nazwa_firmy', $nazwa_firmy)->first();
        if ($clients) {
            return $res->withStatus(409);
        }

        $client = new Client();
        $client->setImie($imie);
        $client->setNazwisko($nazwisko);
        $client->setNazwaFirmy($nazwa_firmy);
        $client->setNumerTelefonu($numer_telefonu);
        $client->setEmail($email);
        $table->insert($client());
        return $res->withStatus(200)->withJson($table->orderByDesc('id_klienta')->first());
    }
    function getClient(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('klienci');
        $responseTable = $table->where('id_klienta', $args['id'])->first();
        return $res->withStatus(200)->withJson($responseTable);
    }
    ///address
    function getAddresses(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('adresy');
        return $res->withStatus(200)->withJson($table->orderBy('id_klienta')->get());
    }
    function getAddress(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('adresy');
        return $res->withStatus(200)->withJson($table->where('id_klienta', $args['id'])->orderBy('id_klienta')->get());
    }
    function addAddress(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        $id_klienta = $body['_id_klienta'];
        $adres_dostawy= $body['_adres_dostawy'];
        $adres_platnosci = $body['_adres_platnosci'];
        if (!$adres_platnosci || !$adres_dostawy || !$id_klienta) {
            return $res->withStatus(400)->withJson($body);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('adresy');

        $address = new Address();
        $address->setIdKlienta(intval($id_klienta));
        $address->setAdresDostawy($adres_dostawy);
        $address->setAdresPlatnosci($adres_platnosci);
        $table->insert($address());
        return $res->withStatus(200)->withJson($address());
    }
    ///orders
    function getOrders(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('zamowienia');
        $responseTable = $table->orderBy('id_zamowienia')->get();
        $orders = [];
        foreach ($responseTable as $item) {
            $order = Order::createFromResponse($item);
            array_push($orders, $order());
        }
        return $res->withStatus(200)->withJson($orders);
    }
    function addOrder(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        $id_klienta = $body['_id_klienta'];
        $cena = $body['_cena'];
        $data_dostawy = $body['_data_dostawy'];
        $adres_dostawy= $body['_adres_dostawy'];
        $adres_platnosci = $body['_adres_platnosci'];
        $uwagi = $body['_uwagi'];
        $data_platnosci = $body['_data_platnosci'];
        if (!$id_klienta || !$cena || !$data_dostawy || !$adres_dostawy || !$adres_platnosci) {
            return $res->withStatus(400)->withJson($body);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('zamowienia');
        $order = new Order();
        $order->setIdKlienta(intval($id_klienta));
        $order->setCena(floatval($cena));
        $order->setDataDostawy(new DateTime($data_dostawy));
        $order->setDataPlatnosci(new DateTime($data_platnosci));
        $order->setAdresPlatnosci($adres_platnosci);
        $order->setAdresDostawy($adres_dostawy);
        $order->setCzyZaplacone(false);
        $order->setUwagi($uwagi);
        $table->insert($order());
        $order = Order::createFromResponse($table->orderByDesc('id_zamowienia')->first());
        return $res->withStatus(200)->withJson($order());

    }
    function updateOrder(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        $id = intval($body['id_zamowienia']);
        $zaplacone = filter_var($body['czy_zaplacone'], FILTER_VALIDATE_BOOLEAN);
        $container
            ->get('db')
            ->table('zamowienia')
            ->where('id_zamowienia', $id)
            ->update(['czy_zaplacone' => $zaplacone]);
        return $res->withStatus(200);
    }
    ///details
    function getDetails(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('szczegoly');
        $responseTable = $table->where('id_zamowienia', $args['id'])->orderBy('cena')->get();
        $details = [];
        foreach ($responseTable as $item) {
            $detail = Details::createFromResponse($item);
            array_push($details, $detail());
        }
        return $res->withStatus(200)->withJson($details);
    }
    function addDetails(Request $req, Response $res, array $args, ContainerInterface $container): Response
    {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        $body = $req->getParsedBody();
        /** @var Builder $table */
        $table = $container->get('db')->table('szczegoly');
        foreach ($body as $item) {
            $detail = Details::createFromBody($item);
            $table->insert($detail());
        }
        return $res->withStatus(200)->withJson($body);
    }
    ///composition
    function getCompositions(Request $req, Response $res, array $args, ContainerInterface $container): Response {
        if (!$this->checkAuthorization($req, $container, false)) {
            return $res->withStatus(401);
        }
        /** @var Builder $table */
        $table = $container->get('db')->table('sklad');
        $responseTable = $table->orderBy('nazwa_produktu')->get();
        $compositions = [];
        foreach ($responseTable as $item) {
            $composition = new Composition($item);
            array_push($compositions, $composition());
        }
        return $res->withStatus(200)->withJson($compositions);
    }
}
