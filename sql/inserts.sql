INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('zloto', 40);
INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('srebro', 40);
INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('pallad', 40);
INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('miedz', 40);
INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('nikiel', 40);
INSERT INTO zasoby(nazwa_zasobu, ilosc) VALUES ('platyna', 40);


INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 999', 155195, 0);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 750 różowe', 116486.88, 4);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 375 różowe', 58588.75, 4);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 585 białe platynowe', 132239.28, 4);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 585 białe niklowe', 90809.83, 4);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Au 400 białe srebrne', 63218, 4);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Ag 999', 1900, 0);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Ag 925', 1821.25, 8);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Ag 958', 1759.38, 10);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Pt 999', 99880, 0);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Pt 800', 110861.55, 6);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Pd 999', 154652, 0);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Ni 999', 50, 0);
INSERT INTO produkty(nazwa_produktu, cena, czas_produkcji) VALUES ('Cu 999', 25, 0);


INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 999', 1, 0, 0, 0, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 750 różowe', 0.75, 0.045, 0, 0.205, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 375 różowe', 0.375, 0.2, 0, 0.425, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 585 białe platynowe', 0.585, 0, 0, 0, 0, 0.415);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 585 białe niklowe', 0.585, 0, 0, 0, 0.415, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Au 400 białe srebrne', 0.4, 0.6, 0, 0, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Ag 999', 0, 1, 0, 0, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Ag 925', 0, 0.925, 0, 0.075, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Ag 958', 0, 0.958, 0, 0, 0.042, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Pt 999', 0, 0, 0, 0, 0, 1);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Pt 800', 0.05, 0, 0.15, 0, 0, 0.8);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Pd 999', 0, 0, 1, 0, 0, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Ni 999', 0, 0, 0, 0, 1, 0);
INSERT INTO sklad(nazwa_produktu, zloto, srebro, pallad, miedz, nikiel, platyna) VALUES ('Cu 999', 0, 0, 0, 1, 0, 0);
