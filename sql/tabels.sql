DROP TABLE zasoby;
CREATE TABLE zasoby (
	nazwa_zasobu varchar(30) NOT NULL PRIMARY KEY,
	ilosc float(30) NOT NULL
);

DROP TABLE produkty;
CREATE TABLE produkty (
	nazwa_produktu varchar(30) NOT NULL PRIMARY KEY,
	cena float(10) NOT NULL,
	czas_produkcji float(10) NOT NULL
);

DROP TABLE sklad;
CREATE TABLE sklad (
	nazwa_produktu VARCHAR(30) NOT NULL PRIMARY KEY,
	zloto FLOAT(10) NOT NULL,
	srebro FLOAT(10) NOT NULL,
	pallad FLOAT(10) NOT NULL,
	miedz FLOAT(10) NOT NULL,
	nikiel FLOAT(10) NOT NULL,
	platyna FLOAT(10) NOT NULL
);

DROP TABLE klienci;
CREATE TABLE klienci (
	id_klienta SERIAL PRIMARY KEY,
	imie VARCHAR(30) NOT NULL,
	nazwisko VARCHAR(30) NOT NULL,
	nazwa_firmy VARCHAR(120),
	numer_telefonu VARCHAR(15),
	email VARCHAR(50),
	data_rejestracji TIMESTAMP default current_timestamp
);

DROP TABLE adresy;
CREATE TABLE adresy (
	id_klienta INT NOT NULL,
	adres_dostawy VARCHAR(300) NOT NULL,
	adres_platnosci VARCHAR(300) NOT NULL
);

DROP TABLE zamowienia;
CREATE TABLE zamowienia (
	id_zamowienia SERIAL PRIMARY KEY,
	id_klienta INT NOT NULL,
	cena FLOAT(30) NOT NULL,
	data_dostawy DATE,
	data_platnosci DATE,
	adres_dostawy VARCHAR(300) NOT NULL,
	adres_platnosci VARCHAR(300) NOT NULL,
	czy_zaplacone BOOLEAN,
	uwagi VARCHAR(1500),
	data_zamowienia TIMESTAMP default current_timestamp
);

DROP TABLE uzytkownicy;
CREATE TABLE uzytkownicy (
	login VARCHAR(30) NOT NULL PRIMARY KEY,
	haslo VARCHAR(120) NOT NULL,
	bearer VARCHAR(60) NOT NULL,
	administrator BOOLEAN
);

DROP TABLE szczegoly;
CREATE TABLE szczegoly (
	id_zamowienia INT NOT NULL,
	nazwa_produktu VARCHAR(30) NOT NULL,
	cena FLOAT(30) NOT NULL,
	ilosc FLOAT(30) NOT NULL
);
